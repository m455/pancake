# pancake

**Pan**do**c** + R**a**c**ke**t = pancake

An awful static website generator with an awful logo and an awful acronym.

![A horribly-drawn pancake](images/logo.png)

## Table of Contents

- [Disclaimer](#disclaimer)
- [Conventions used in this document](#conventions-used-in-this-document)
- [Requirements](#requirements)
- [Quick start](#quick-start)
- [Downloading pancake](#downloading-pancake)
    - [Downloading pancake using git](#downloading-pancake-using-git)
        - [To download pancake using git](#to-download-pancake-using-git)
- [Installing pancake](#installing-pancake)
    - [Installing pancake globally](#installing-pancake-globally)
        - [To install pancake globally](#to-install-pancake-globally)
    - [Installing pancake locally](#installing-pancake-locally)
        - [To install pancake locally](#to-install-pancake-locally)
    - [Installing pancake to a custom directory](#installing-pancake-to-a-custom-directory)
        - [To install pancake to a custom directory](#to-install-pancake-to-a-custom-directory)
- [Uninstalling pancake](#uninstalling-pancake)
    - [Uninstalling pancake globally](#uninstalling-pancake-globally)
        - [To uninstall pancake globally](#to-uninstall-pancake-globally)
    - [Uninstalling pancake locally](#uninstalling-pancake-locally)
        - [To uninstall pancake locally](#to-uninstall-pancake-locally)
- [Using pancake](#using-pancake)
    - [Displaying the help message](#displaying-the-help-message)
        - [To display the help message](#to-display-the-help-message)
    - [Initializing pancake](#initializing-pancake)
        - [To initialize pancake](#to-initialize-pancake)
    - [Building your website](#building-your-website)
        - [To build your website](#to-build-your-website)
    - [Previewing your website](#previewing-your-website)
        - [To preview your website](#to-preview-your-website)
    - [Cleaning your website](#cleaning-your-website)
        - [To clean your website](#to-clean-your-website)
- [Understanding pancake's directories](#understanding-pancakes-directories)
    - [The css directory](#the-css-directory)
    - [The images directory](#the-images-directory)
    - [The misc directory](#the-misc-directory)
    - [The pdfs directory](#the-pdfs-directory)
    - [The drafts directory](#the-drafts-directory)
    - [The output directory](#the-output-directory)
    - [The pages directory](#the-pages-directory)
    - [The posts directory](#the-posts-directory)
    - [The layout directory](#the-layout-directory)
        - [The head.md file](#the-headmd-file)
        - [The nav.md file](#the-navmd-file)
        - [The footer.md file](#the-footermd-file)

## Disclaimer

This is a hobby project I built for myself as a tool to make blogging more enjoyable.

I take no responsibility for anything that pancake deletes.

Backup anything you don't want deleted.

## Conventions used in this document

* **Note**: Notes signify additional information
* **Tip**: Tips signify an alternate procedure for completing a step
* **Warning**: Warnings signify that damage may occur
* **Example**: Examples provide a visual reference of how a procedure would be carried out in the real world
* `Inline code`: Inline code signifies package names, filenames, or commands
* `Code block`: Code blocks signify file contents

## Requirements

* Racket
* Pandoc

## Quick start

This section is for users who are familiar with git, a Unix-like command line environment, or
scripting.

1. Make sure [Racket](https://racket-lang.org/) and [Pandoc](https://pandoc.org/) are installed
2. `git clone https://git.m455.casa/m455/pancake.git`
3. `cd pancake`
4. `sudo make install-global`
5. `pancake`

**Note**: To uninstall, run `sudo make uninstall-global`

* `pancake help`       - Displays this help message
* `pancake initialize` - Generates the directories and files in your current directory, so pancake can operate
* `pancake build`      - Builds your website in the `output/` directory
* `pancake clean`      - Deletes the contents of the `output/` directory, except for any `.git` directories found.
* `pancake preview`    - Starts a local HTTP server in your output/ directory to view your website

## Downloading pancake

pancake's source code exists in a public git repository. This makes
accessing the code convenient, because you don't need to sign in or
register for an account to download it.

### Downloading pancake using git

You can use
tools such as `git` to download pancake's source code. You will need
the source code to install pancake.

#### To download pancake using git

1. Run `git clone https://git.m455.casa/m455/pancake.git`

**Note**: This will create a `pancake` directory in your current directory.

## Installing pancake

You can either install pancake globally or locally on your system. A
global installation allows all users on a machine to use pancake,
while a local installation only allows one user to use pancake.

See the options below for installing pancake:

* [Installing pancake globally](#installing-pancake-globally)
* [Installing pancake locally](#installing-pancake-locally)
* [Installing pancake to a custom directory](#installing-pancake-to-a-custom-directory)

### Installing pancake globally

This option will install pancake into `/usr/local/bin/`.

This section assumes you have [downloaded pancake](#downloading-pancake).

#### To install pancake globally

1. Run `cd pancake`
2. Run `sudo make install-global`

### Installing pancake locally

This option will install pancake into `~/.local/bin/`.

This section assumes you have [downloaded pancake](#downloading-pancake).

#### To install pancake locally

1. Run `cd pancake`
2. Run `sudo make install-local`

### Installing pancake to a custom directory

If you wish to have pancake exist elsewhere on your system, you can
also build a single-file executable. Building a single-file executable
allows you to place the executable in convenient places on your
system, such as a directory on your `$PATH`.

This section assumes you have [downloaded pancake](#downloading-pancake).

#### To install pancake to a custom directory

**Warning**: You will have to manually uninstall custom installations

1. Run `cd pancake`
2. Run `make install-custom location=~/path/to/custom/location`

**Example**: In step 2., you could run `make install-custom location=~/bin/`

## Uninstalling pancake

Depending on your installation method, you can uninstall a global or local installation of pancake.

See the options below for uninstalling pancake:

* [Uninstalling pancake globally](#uninstalling-pancake-globally)
* [Uninstalling pancake locally](#uinstalling-pancake-locally)

### Uninstalling pancake globally

This option will remove the pancake executable file from `/usr/local/bin/`.

This section assumes you have [downloaded pancake](#downloading-pancake).

#### To uninstall pancake globally

1. Run `cd pancake`
2. Run `sudo make uninstall-global`

### Uninstalling pancake locally

This option will remove the pancake executable file from `~/.local/bin/`.

This section assumes you have [downloaded pancake](#downloading-pancake).

#### To uninstall pancake locally

1. Run `cd pancake`
2. Run `sudo make uninstall-local`

## Using pancake

pancake can generate HTML files from a set of Markdown files in specific directories, repair missing
directories or files, and preview your website before deploying it.

### Displaying the help message

Pancake provides a small help message, for further inquiries, refer to this `README.md`.

#### To display the help message

1. Run `pancake help`

### Initializing pancake

Before using pancake, you need to 'initialize' pancake in a directory
of your choice. 'initialize' just means that pancake will create the
required directories and files it needs before running.

#### To initialize pancake

1. Run `pancake initialize`

**Note**: This command will generate directories and files in your current directory.

### Building your website

When pancake 'builds', it will:

* Convert all Markdown files in the `pages/` directory, and place them in `output/`
* Convert all Markdown files in the `posts/` directory, and place them in `output/posts/`
* Copy all files in the `pdfs/` to `output/pdfs/`
* Copy all files in the `images/` to `output/images/`
* Copy all files in the `css/` to `output/css/`
* Copy all files in the `misc/` to `output/misc/`

#### To build your website

1. Run `pancake build`

### Previewing your website

pancake can create a temporary HTTP server, so you can preview your
website before it goes public.

#### To preview your website

1. Run `pancake preview`

### Cleaning your website

pancake can 'clean' your `output/` directory. This means pancake will
delete all contents of your `output/` directory, except for any `.git`
directories it finds.

#### To clean your website

1. Run `pancake clean`

## Understanding pancake's directories

pancake initializes several directories at first. This section will
discuss the significance of each of the following directories, and how
to use them:

* [The css directory](#the-css-directory)
* [The images directory](#the-images-directory)
* [The misc directory](#the-misc-directory)
* [The pdfs directory](#the-pdfs-directory)
* [The drafts directory](#the-drafts-directory)
* [The output directory](#the-output-directory)
* [The pages directory](#the-pages-directory)
* [The posts directory](#the-posts-directory)
* [The layout directory](#the-layout-directory)

### The css directory

This directory contains CSS files created by the user. CSS files in
this directory will automatically be applied to all HTML files that
are generated inside the `output/` and `output/posts` directories.

**Note**: HTML files in the `output/misc` directory are not affected
by CSS files in the `css/` directory.

When you run `pancake build`, pancake copies the CSS files in
`pdfs/` to `output/pdfs/`. pandoc adds a link to each of these CSS
files in headers of each HTML file inside of the `output/` and
`output/posts` directories.

### The images directory

This directory contains image files added by the user. When
referencing an image from `images/` in a Markdown file, always prefix
it with `/images/`.

**Example**: `/images/selfie.png`

When you run `pancake build`, pancake copies the image files in
`images/` to `output/images/`.

### The misc directory

The `misc/` directory is for storing any kind of file publicly. When
referencing a file from `misc/` in a Markdown file, always prefix it
with `/misc/`.

**Example**: `/misc/my-text-file.txt`

This directory is for storing one-off files, such as an HTML file with
its own styles, or a plain text file, or even a temporary file you
want to share with a friend. I currently use the `misc/` directory to
store my PGP key, which is then reference on my `contact.html` page,
from where people can download it.

When you run `pancake build`, pancake copies the files in the
`misc/` to `output/misc/`.

### The pdfs directory

This directory contains PDF files added by the user. When referencing
a PDF file from `pdfs/` in a Markdown file, always prefix it with
`/pdfs/`.

**Example**: `/pdfs/white-paper.png`

When you run `pancake build`, pancake copies the PDF files in
`pdfs/` to `output/pdfs/`.

### The drafts directory

This directory contains files for later use. Pancake ignores any files
inside of this directory.

### The output directory

The `output/` directory is where your website is generated. You
shouldn't edit anything in the `output/` directory, because the
directories or files will be replaced each time you run `pancake
build`.

### The pages directory

The `pages/` directory contains Markdown files that will be accessible
from the root directory of your website after being generated.

**Example**: `yourwebsite.com/a-page-file.html`

This directory is intended for pages that are accessible from the nav
links inside of the `layout/nav.md` file, but you can use it however
you want.

When you run `pancake build`, the Markdown files in `pages/` will be
converted to HTML files and copied to `output/`.

### The posts directory

The `posts/` directory contains Markdown files that will be accessible
from the `posts/` directory inside of the root directory of your
website after being generated.

**Example**: `yourwebsite.com/posts/2020-blog-post.html`

When you run `pancake build`, the Markdown files in `posts/` will be
converted to HTML files and copied to `output/posts/`.

### The layout directory

This directory contains the building blocks of each web page. The
`layout/head.md` file should be edited to add the title, an optional
subtitle, and optional keywords related to the topics existing on your
website.

#### The head.md file

The value after `title:` is mandatory, and should remain in single
quotes if you plan on having a colon in your title. If you plan on
having single quotes in your title, you have to escape them with a
`\`.

**Example**: `title: 'Sherry\'s blog'`

The value after `subtitle:` is optional, and follows the same rules as
the `title:` value, but you can leave it blank if you don't want a
`subtitle:` value, or you can delete the `subtitle:` line.

**Example**: `subtitle: 'They/them'`

**Note**: I originally added the `subtitle:` value, so I could include
my pronouns on my homepage, but you can change it to other values.

The value after `keywords:` is optional, and should remain in
brackets, if it is not blank. Inside of the brackets, you should
include keywords related to topics mentioned on your website. These
keywords should be separated by commas.

**Example**: `keywords: [programming, documentation, personal homepages]`

#### The nav.md file

The default contents of the `nav.md` file imply that it is used for a
navigation bar, but this could be anything you want, or it could be
blank if you don't want anything.

The default `::: {#main}` sections off the rest of the website that is
stitched below it, during a `pancake build`, causing the main
content to exist inside of a `#main` id for styling purposes.

#### The footer.md file

The default contents of the `footer.md` file contain a message saying
your website was created by pancake, but this is not mandatory. You
can change the footer to anything you want.
